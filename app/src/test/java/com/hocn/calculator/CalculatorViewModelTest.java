package com.hocn.calculator;

import android.app.Application;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.hocn.calculator.db.IStorage;
import com.hocn.calculator.demo.model.CalculatorModel;
import com.hocn.calculator.demo.viewmodel.CalculatorViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorViewModelTest {
    @Mock
    private Application mockApplicationContext;

    @Mock
    private IStorage mStorage;

    private CalculatorViewModel mViewModel;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule =
            new InstantTaskExecutorRule();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mViewModel = new CalculatorViewModel(mockApplicationContext, mStorage);
    }

    @Test
    public void testCalculator1() {
        mViewModel.inputNumber("5");
        mViewModel.inputOperation(CalculatorModel.Operation.ADD);
        mViewModel.inputNumber("6");
        mViewModel.inputEqual();
        Assert.assertEquals(mViewModel.getCalculator().getResultDisplay(), "11");
    }

    @Test
    public void testCalculator2() {
        mViewModel.inputNumber("1");
        mViewModel.inputNumber("5");
        mViewModel.inputOperation(CalculatorModel.Operation.ADD);
        mViewModel.inputNumber("1");
        mViewModel.inputNumber("6");
        mViewModel.inputEqual();
        Assert.assertEquals(mViewModel.getCalculator().getResultDisplay(), "31");
    }

    @Test
    public void testCalculator3() {
        mViewModel.inputNumber("1");
        mViewModel.inputOperation(CalculatorModel.Operation.ADD);
        mViewModel.inputNumber("5");
        mViewModel.inputOperation(CalculatorModel.Operation.MULTIPLY);
        mViewModel.inputNumber("2");
        mViewModel.inputEqual();
        Assert.assertEquals(mViewModel.getCalculator().getResultDisplay(), "12");
    }

    @Test
    public void testCalculator4() {
        mViewModel.inputNumber("1");
        mViewModel.inputOperation(CalculatorModel.Operation.ADD);
        mViewModel.inputNumber("5");
        mViewModel.inputEqual();

        mViewModel.inputNumber("4");
        mViewModel.inputOperation(CalculatorModel.Operation.ADD);
        mViewModel.inputNumber("3");
        mViewModel.inputEqual();

        Assert.assertEquals(mViewModel.getCalculator().getResultDisplay(), "7");
    }
}
