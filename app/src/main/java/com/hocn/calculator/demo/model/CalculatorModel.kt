package com.hocn.calculator.demo.model

class CalculatorModel {
    private var leftValue: Double? = 0.0
    private var rightValue: Double? = 0.0
    private var operation: Operation? = null
    private var result: Double? = 0.0

    constructor() {}

    constructor(leftValue: Double?, rightValue: Double?, operation: Operation, result: Double?) {
        this.leftValue = leftValue
        this.rightValue = rightValue
        this.operation = operation
        this.result = result
    }

    enum class NumberType {
        NONE,
        NUMBER,
        OPERATION,
        EQUAL
    }

    enum class Operation {
        ADD, SUBTRACT, MULTIPLY, DIVIDE
    }

    override fun toString(): String {
        var operatorString = ""

        when (operation) {
            Operation.ADD -> operatorString = " + "

            Operation.SUBTRACT -> operatorString = " - "

            Operation.MULTIPLY -> operatorString = " * "

            Operation.DIVIDE -> operatorString = " : "
        }

        return leftValue.toString() + operatorString + rightValue + " = " + result
    }
}
