package com.hocn.calculator.demo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.hocn.calculator.db.IStorage;

public class CalculatorViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private IStorage mStorage;

    public CalculatorViewModelFactory(Application application, IStorage storage) {
        mApplication = application;
        mStorage = storage;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CalculatorViewModel(mApplication, mStorage);
    }
}
