package com.hocn.calculator.demo.di

import android.content.Context
import com.hocn.calculator.db.IStorage
import com.hocn.calculator.db.InMemoryStorage
import com.hocn.calculator.helper.IPreferenceManager
import com.hocn.calculator.helper.PreferenceManager

object DIContainer {
    fun getStorage(context: Context?): IStorage {
        return InMemoryStorage(getPreferenceManager(context))
    }

    private fun getPreferenceManager(context: Context?): IPreferenceManager {
        return PreferenceManager(context)
    }
}