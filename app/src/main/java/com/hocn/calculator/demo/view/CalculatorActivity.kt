package com.hocn.calculator.demo.view

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.hocn.calculator.R
import com.hocn.calculator.demo.di.DIContainer
import com.hocn.calculator.demo.model.CalculatorModel
import com.hocn.calculator.demo.viewmodel.CalculatorViewModel
import com.hocn.calculator.demo.viewmodel.CalculatorViewModelFactory

class CalculatorActivity : AppCompatActivity() {

    private var viewModel: CalculatorViewModel? = null

    private var buttonNumber1: Button? = null
    private var buttonNumber2: Button? = null
    private var buttonNumber3: Button? = null
    private var buttonNumber4: Button? = null
    private var buttonNumber5: Button? = null
    private var buttonNumber6: Button? = null
    private var buttonNumber7: Button? = null
    private var buttonNumber8: Button? = null
    private var buttonNumber9: Button? = null
    private var buttonNumber0: Button? = null

    private var buttonAdd: Button? = null
    private var buttonSubtract: Button? = null
    private var buttonMultiple: Button? = null
    private var buttonDivide: Button? = null
    private var buttonEqual: Button? = null
    private var textResult: TextView? = null

    private var buttonShowHistory: Button? = null
    private var buttonClearHistory: Button? = null
    private var textHistory: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        // Init view model
        initViewModel()

        // Find view
        findView()

        // Bind event
        bindEvent()

        // Obs viewmodel
        obsViewModel()
    }

    private fun initViewModel() {
        val factory = CalculatorViewModelFactory(
            application,
            DIContainer.getStorage(this)
        )
        viewModel = ViewModelProvider(this, factory)
            .get(CalculatorViewModel::class.java)
    }

    private fun obsViewModel() {
        viewModel?.obsTextResult
            ?.observe(this,
                Observer<String?> { s -> textResult!!.text = s })

        viewModel?.obsTextHistory
            ?.observe(this,
                Observer<String?> { s -> textHistory!!.text = s })
    }

    private fun findView() {
        buttonNumber0 = findViewById(R.id.buttonNumber0)
        buttonNumber1 = findViewById(R.id.buttonNumber1)
        buttonNumber2 = findViewById(R.id.buttonNumber2)
        buttonNumber3 = findViewById(R.id.buttonNumber3)
        buttonNumber4 = findViewById(R.id.buttonNumber4)
        buttonNumber5 = findViewById(R.id.buttonNumber5)
        buttonNumber6 = findViewById(R.id.buttonNumber6)
        buttonNumber7 = findViewById(R.id.buttonNumber7)
        buttonNumber8 = findViewById(R.id.buttonNumber8)
        buttonNumber9 = findViewById(R.id.buttonNumber9)
        buttonAdd = findViewById(R.id.buttonAdd)
        buttonSubtract = findViewById(R.id.buttonSubtract)
        buttonMultiple = findViewById(R.id.buttonMultiple)
        buttonDivide = findViewById(R.id.buttonDivide)
        buttonEqual = findViewById(R.id.buttonEqual)
        textResult = findViewById(R.id.textResult)
        buttonShowHistory = findViewById(R.id.buttonShowHistory)
        buttonClearHistory = findViewById(R.id.buttonClearHistory)
        textHistory = findViewById(R.id.textHistory)
    }

    private fun bindEvent() {
        buttonNumber0!!.setOnClickListener(mOnClickListener)
        buttonNumber1!!.setOnClickListener(mOnClickListener)
        buttonNumber2!!.setOnClickListener(mOnClickListener)
        buttonNumber3!!.setOnClickListener(mOnClickListener)
        buttonNumber4!!.setOnClickListener(mOnClickListener)
        buttonNumber5!!.setOnClickListener(mOnClickListener)
        buttonNumber6!!.setOnClickListener(mOnClickListener)
        buttonNumber7!!.setOnClickListener(mOnClickListener)
        buttonNumber8!!.setOnClickListener(mOnClickListener)
        buttonNumber9!!.setOnClickListener(mOnClickListener)
        buttonAdd!!.setOnClickListener(mOnClickListener)
        buttonSubtract!!.setOnClickListener(mOnClickListener)
        buttonMultiple!!.setOnClickListener(mOnClickListener)
        buttonDivide!!.setOnClickListener(mOnClickListener)
        buttonEqual!!.setOnClickListener(mOnClickListener)
        buttonShowHistory!!.setOnClickListener(mOnClickListener)
        buttonClearHistory!!.setOnClickListener(mOnClickListener)
    }

    private val mOnClickListener =
        View.OnClickListener { v ->
            when (v.id) {
                R.id.buttonNumber0 -> {
                    viewModel?.inputNumber("0")
                }
                R.id.buttonNumber1 -> {
                    viewModel?.inputNumber("1")
                }
                R.id.buttonNumber2 -> {
                    viewModel?.inputNumber("2")
                }
                R.id.buttonNumber3 -> {
                    viewModel?.inputNumber("3")
                }
                R.id.buttonNumber4 -> {
                    viewModel?.inputNumber("4")
                }
                R.id.buttonNumber5 -> {
                    viewModel?.inputNumber("5")
                }
                R.id.buttonNumber6 -> {
                    viewModel?.inputNumber("6")
                }
                R.id.buttonNumber7 -> {
                    viewModel?.inputNumber("7")
                }
                R.id.buttonNumber8 -> {
                    viewModel?.inputNumber("8")
                }
                R.id.buttonNumber9 -> {
                    viewModel?.inputNumber("9")
                }
                R.id.buttonAdd -> {
                    viewModel?.inputOperation(CalculatorModel.Operation.ADD)
                }
                R.id.buttonSubtract -> {
                    viewModel?.inputOperation(CalculatorModel.Operation.SUBTRACT)
                }
                R.id.buttonMultiple -> {
                    viewModel?.inputOperation(CalculatorModel.Operation.MULTIPLY)
                }
                R.id.buttonDivide -> {
                    viewModel?.inputOperation(CalculatorModel.Operation.DIVIDE)
                }
                R.id.buttonEqual -> {
                    viewModel?.inputEqual()
                }
                R.id.buttonShowHistory -> {
                    viewModel?.showHistory()
                }
                R.id.buttonClearHistory -> {
                    viewModel?.clearHistory()
                }
            }
        }
}
