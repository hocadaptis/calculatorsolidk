package com.hocn.calculator.demo.viewmodel

import com.hocn.calculator.db.IStorage
import com.hocn.calculator.demo.model.CalculatorModel
import java.text.DecimalFormat

class Calculator(private val mStorage: IStorage) {

    private var valNumber1: Double? = null
    private var valNumber2: Double? = null
    private var result: Double? = 0.0
    private var operation: CalculatorModel.Operation? = null
    var lastNumberType = CalculatorModel.NumberType.NONE

    val resultDisplay: String
        get() {
            if (result == null) {
                return ""
            }

            val format = DecimalFormat("0.#")
            return format.format(result!!)
        }

    fun inputNumber(number: Double?) {
        if (valNumber1 == null) {
            valNumber1 = number
        } else {
            valNumber2 = number
        }
    }

    fun inputOperation(operation: CalculatorModel.Operation?) {

        if (lastNumberType === CalculatorModel.NumberType.OPERATION) {
            this.operation = operation
            return
        }

        if (lastNumberType === CalculatorModel.NumberType.EQUAL) {
            this.operation = operation
            valNumber1 = result
            lastNumberType = CalculatorModel.NumberType.OPERATION
            return
        }

        if (valNumber2 == null) {
            this.operation = operation
            result = valNumber1
            lastNumberType = CalculatorModel.NumberType.OPERATION
            return
        }

        result = executeCal(getNumberNotNull(valNumber1), getNumberNotNull(valNumber2), this.operation)
        this.operation = operation
        valNumber1 = result
        valNumber2 = null
        lastNumberType = CalculatorModel.NumberType.OPERATION
    }

    fun inputEqual() {
        if (lastNumberType === CalculatorModel.NumberType.EQUAL) {
            return
        }

        result = executeCal(getNumberNotNull(valNumber1), getNumberNotNull(valNumber2), operation!!)
        valNumber1 = null
        valNumber2 = null

        lastNumberType = CalculatorModel.NumberType.EQUAL
    }

    private fun getNumberNotNull(number: Double?): Double? {
        return number ?: 0.0
    }

    private fun executeCal(a: Double?, b: Double?, operation: CalculatorModel.Operation?): Double? {
        if (operation == null) {
            return null
        }

        var result: Double? = null

        result = when (operation) {
            CalculatorModel.Operation.ADD -> a!! + b!!
            CalculatorModel.Operation.SUBTRACT -> a!! - b!!
            CalculatorModel.Operation.MULTIPLY -> a!! * b!!
            CalculatorModel.Operation.DIVIDE -> if (b != 0.0) {
                a!! / b!!
            } else {
                return null
            }
        }

        // Save data
        mStorage.saveData(CalculatorModel(a, b, operation, result))

        return result
    }
}
