package com.hocn.calculator.demo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.hocn.calculator.db.IStorage
import com.hocn.calculator.demo.model.CalculatorModel

class CalculatorViewModel(application: Application, storage: IStorage) :
    AndroidViewModel(application) {

    private val mStorage: IStorage = storage
    val calculator: Calculator = Calculator(storage)

    val obsTextResult =
        MutableLiveData<String?>()
    val obsTextHistory = MutableLiveData<String>()

    private var displayNumber: String? = null

    fun inputNumber(number: String?) {
        if (calculator.lastNumberType === CalculatorModel.NumberType.NUMBER) {
            displayNumber += number
        } else {
            displayNumber = number
        }
        setValue(displayNumber)
        calculator.lastNumberType = CalculatorModel.NumberType.NUMBER
    }

    fun inputOperation(operation: CalculatorModel.Operation?) {
        calculator.inputNumber(currentNumber)
        calculator.inputOperation(operation)
        setValue(calculator.resultDisplay)
    }

    fun inputEqual() {
        calculator.inputNumber(currentNumber)
        calculator.inputEqual()
        setValue(calculator.resultDisplay)
    }

    fun showHistory() {
        val history: String = mStorage.getData()
        obsTextHistory.value = history
    }

    fun clearHistory() {
        mStorage.clearData()
        obsTextHistory.value = ""
    }

    private val currentNumber: Double
        get() = if (displayNumber!!.isEmpty()) 0.0 else java.lang.Double.valueOf(displayNumber!!)

    private fun setValue(text: String?) {
        obsTextResult.value = text
    }
}