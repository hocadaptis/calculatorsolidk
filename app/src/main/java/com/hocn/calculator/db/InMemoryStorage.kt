package com.hocn.calculator.db

import com.hocn.calculator.demo.model.CalculatorModel
import com.hocn.calculator.helper.IPreferenceManager

class InMemoryStorage(private val preferenceManager: IPreferenceManager) : IStorage {

    override fun saveData(model: CalculatorModel) {
        val currentHistory = getData()
        val newHistory = "$currentHistory ; $model"
        preferenceManager.saveCalculatorResult(newHistory)
    }

    override fun getData(): String {
        return preferenceManager.getCalculatorResult()
    }

    override fun clearData() {
        preferenceManager.clearCalculatorResult()
    }
}
