package com.hocn.calculator.db

import com.hocn.calculator.demo.model.CalculatorModel

interface IStorage {
    fun saveData(model: CalculatorModel)
    fun getData(): String
    fun clearData()
}
