package com.hocn.calculator.helper

interface IPreferenceManager {
    fun saveCalculatorResult(result: String)
    fun getCalculatorResult(): String
    fun clearCalculatorResult()
}
