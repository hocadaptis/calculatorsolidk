package com.hocn.calculator.helper

import android.content.Context
import android.content.SharedPreferences

const val APP_PREFERENCE_NAME = "calculator"
const val PREF_CALCULATOR_RESULT = "PREF_CALCULATOR_RESULT"

class PreferenceManager(context: Context?) : IPreferenceManager {

    private var sharedPreferences: SharedPreferences? = null

    init {
        this.sharedPreferences = context?.getSharedPreferences(
            APP_PREFERENCE_NAME,
            Context.MODE_PRIVATE
        )
    }

    override fun saveCalculatorResult(result: String) {
        val editor = sharedPreferences?.edit()
        editor?.putString(PREF_CALCULATOR_RESULT, result)
        editor?.commit()
    }

    override fun getCalculatorResult(): String {
        return sharedPreferences?.getString(PREF_CALCULATOR_RESULT, "")!!
    }

    override fun clearCalculatorResult() {
        val editor = sharedPreferences?.edit()
        editor?.remove(PREF_CALCULATOR_RESULT)
        editor?.commit()
    }
}
