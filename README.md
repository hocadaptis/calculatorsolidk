

## **Calculator App Demo**

**This application is a simple calculator which contains following features:**  
- Take user's input as a standard calculator, including addition, subtraction, multiplication and division  

- Base on user's input, validate expression, then output the result  

- Allow user to save the result for future usage  

- Allow user to retrive saved result

**The app has following packages:**
- view, viewmodel, model: Fllowing MVVM design pattern

- di: Contain dependency injection container class

- db: Database manager 

- helper: Contain helper class

- testcase: Testcase is written in class CalculatorViewModelTest


**The code follow SOLID principle**

- (S): Each module has single responsibility: Calculator just for calculate, InMemoryStorage just for save data, Calculator Activity just for show UI and catch Event, ...

- (O): Now we are using Preference to save data, but we can use any kind of Database (Realm or remote datase). We just need to implement IStorage interface

- (D): 
We need to save user action history but CalculatorViewModel doesn't depend on any kind of storage. It's depend on IStorage interface. We inject kind of storage in DI Container

	     object DIContainer {
          fun getStorage(context: Context?): IStorage {
            return InMemoryStorage(getPreferenceManager(context))
          }
    
          private fun getPreferenceManager(context: Context?): IPreferenceManager {
            return PreferenceManager(context)
          }
        }

CalculatorViewModel doesn't depend on Android View. View can observe variable from ViewModel to change UI (We are using LiveData for that)

**Testcase**

Following SOLID principle, we can test CalculatorViewModel without care about View or Storage.
We're using mockito library

    @Mock  
    private Application mockApplicationContext;  
      
    @Mock  
    private IStorage mStorage;  
      
    private CalculatorViewModel mViewModel;  
      
    @Rule  
    public InstantTaskExecutorRule instantTaskExecutorRule =  
            new InstantTaskExecutorRule();  
      
    @Before  
    public void setUp() throws Exception {  
        MockitoAnnotations.initMocks(this);  
        mViewModel = new CalculatorViewModel(mockApplicationContext, mStorage);  
    }
